/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef __TRKVERTEXWEIGHTCALCULATORS_DICT__
#define __TRKVERTEXWEIGHTCALCULATORS_DICT__
#include "TrkVertexWeightCalculators/BDTVertexWeightCalculator.h"
#include "TrkVertexWeightCalculators/DecorateVertexScoreAlg.h"
#endif


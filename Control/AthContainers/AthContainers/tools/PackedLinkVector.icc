/**
 * @file AthContainers/tools/PackedLinkVector.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2023
 * @brief Implementation of @c IAuxTypeVector for @c PackedLink types.
 */


#include "AthContainers/AuxTypeRegistry.h"


namespace SG {


/**
 * @brief Constructor.
 * @param auxid The auxid of the variable this vector represents.
 * @param vecPtr Pointer to the object.
 * @param linkedVec Interface for the linked vector of DataLinks.
 * @param ownFlag If true, take ownership of the object.
 */
template <class CONT, class ALLOC>
PackedLinkVectorHolder<CONT, ALLOC>::PackedLinkVectorHolder
  (auxid_t auxid,
   vector_type* vecPtr,
   IAuxTypeVector* linkedVec,
   bool ownFlag)
    : Base (auxid, vecPtr, ownFlag, false),
      m_linkedVec (linkedVec)
{
}


/**
 * @brief Insert elements into the vector via move semantics.
 * @param pos The starting index of the insertion.
 * @param src Start of the vector containing the range of elements to insert.
 * @param src_pos Position of the first element to insert.
 * @param src_n Number of elements to insert.
 * @param srcStore The source store.
 *
 * @c beg and @c end define a range of container elements, with length
 * @c len defined by the difference of the pointers divided by the
 * element size.
 *
 * The size of the container will be increased by @c len, with the elements
 * starting at @c pos copied to @c pos+len.
 *
 * The contents of the source range will then be moved to our vector
 * starting at @c pos.  This will be done via move semantics if possible;
 * otherwise, it will be done with a copy.
 *
 * Returns true if it is known that the vector's memory did not move,
 * false otherwise.
 */
template <class CONT, class ALLOC>
bool PackedLinkVectorHolder<CONT, ALLOC>::insertMove
   (size_t pos, void* src, size_t src_pos, size_t src_n,
    IAuxStore& srcStore)
{
  bool ret = Base::insertMove (pos, src, src_pos, src_n, srcStore);
  auto srcp = reinterpret_cast<element_type*> (src);
  auto begp = srcp + src_pos;
  auto endp = begp + src_n;
  size_t n = std::distance (begp, endp);
  vector_type& vec = this->vec();

  const IAuxTypeVector* srcLv = srcStore.linkedVector (this->auxid());
  typename Helper::const_DataLinkBase_span srcDLinks = Helper::getLinkBaseSpan (*srcLv);
  ret &= Helper::updateLinks (*m_linkedVec, vec.data()+pos, n,
                              srcDLinks, nullptr);
  return ret;
}


//*****************************************************************************


/**
 * @brief Constructor.
 * @param auxid The auxid of the variable this vector represents.
 * @param vecPtr Pointer to the object (of type @c CONT).
 * @param linkedVec Interface for the linked vector of DataLinks.
 * @param ownFlag If true, take ownership of the object.
 */
template <class CONT, class VALLOC, class VELT, class ALLOC>
PackedLinkVVectorHolder<CONT, VALLOC, VELT, ALLOC>::PackedLinkVVectorHolder
  (auxid_t auxid,
   vector_type* vecPtr,
   IAuxTypeVector* linkedVec,
   bool ownFlag)
    : Base (auxid, vecPtr, ownFlag, false),
      m_linkedVec (linkedVec)
{
}


/**
 * @brief Insert elements into the vector via move semantics.
 * @param pos The starting index of the insertion.
 * @param src Start of the vector containing the range of elements to insert.
 * @param src_pos Position of the first element to insert.
 * @param src_n Number of elements to insert.
 * @param srcStore The source store.
 *
 * @c beg and @c end define a range of container elements, with length
 * @c len defined by the difference of the pointers divided by the
 * element size.
 *
 * The size of the container will be increased by @c len, with the elements
 * starting at @c pos copied to @c pos+len.
 *
 * The contents of the source range will then be moved to our vector
 * starting at @c pos.  This will be done via move semantics if possible;
 * otherwise, it will be done with a copy.
 *
 * Returns true if it is known that the vector's memory did not move,
 * false otherwise.
 */
template <class CONT, class VALLOC, class VELT, class ALLOC>
bool PackedLinkVVectorHolder<CONT, VALLOC, VELT, ALLOC>::insertMove
   (size_t pos, void* src, size_t src_pos, size_t src_n,
    IAuxStore& srcStore)
{
  bool ret = Base::insertMove (pos, src, src_pos, src_n, srcStore);
  auto srcp = reinterpret_cast<element_type*> (src);
  auto begp = srcp + src_pos;
  auto endp = begp + src_n;
  size_t n = std::distance (begp, endp);
  vector_type& vec = this->vec();
  const IAuxTypeVector* srcLv = srcStore.linkedVector (this->auxid());
  typename Helper::const_DataLinkBase_span srcDLinks = Helper::getLinkBaseSpan (*srcLv);

  for (size_t i = pos; i < pos+n; ++i) {
    ret &= Helper::updateLinks (*m_linkedVec, vec[i].data(), vec[i].size(),
                                srcDLinks, nullptr);
  }
  return ret;
}


//*****************************************************************************


/**
 * @brief Constructor.  Makes a new vector.
 * @param auxid The auxid of the variable this vector represents.
 * @param size Initial size of the new vector.
 * @param capacity Initial capacity of the new vector.
 * @param linkedVec Ownership of the linked vector.
 */
template <class HOLDER>
PackedLinkVectorT<HOLDER>::PackedLinkVectorT (auxid_t auxid,
                                              size_t size,
                                              size_t capacity,
                                              std::unique_ptr<IAuxTypeVector> linkedVec)
  : Base (auxid, &m_vec, linkedVec.get(), false),
    m_linkedVecHolder (std::move (linkedVec))
{
  m_vec.reserve (capacity);
  m_vec.resize (size);
}


/**
 * @brief Copy constructor.
 */
template <class HOLDER>
PackedLinkVectorT<HOLDER>::PackedLinkVectorT (const PackedLinkVectorT& other)
  : Base (other.auxid(), &m_vec, false, false),
    m_vec (other.m_vec),
    m_linkedVecHolder (other.m_linkedVec->clone())
{
  Base::m_linkedVec = m_linkedVecHolder.get();
}


/**
 * @brief Move constructor.
 */
template <class HOLDER>
PackedLinkVectorT<HOLDER>::PackedLinkVectorT (PackedLinkVectorT&& other)
  : Base (other.auxid(), &m_vec, false, other.m_linkedVec),
    m_vec (std::move (other.m_vec)),
    m_linkedVecHolder (std::move (other.m_linkedVecHolder))
{
}


/**
 * @brief Make a copy of this vector.
 */
template <class HOLDER>
inline
std::unique_ptr<IAuxTypeVector> PackedLinkVectorT<HOLDER>::clone() const
{
  return std::make_unique<PackedLinkVectorT> (*this);
}


/**
 * @brief Return ownership of the linked vector.
 */
template <class HOLDER>
inline
std::unique_ptr<IAuxTypeVector> PackedLinkVectorT<HOLDER>::linkedVector()
{
  return std::move (m_linkedVecHolder);
}


} // namespace SG
